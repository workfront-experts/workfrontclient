<?php namespace Outbox\Client\Workfront;


use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ParseException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Outbox\Client\Workfront\Exceptions\WorkfrontClientException;
use Outbox\Client\Workfront\Exceptions\WorkfrontHttpExceptionFactory;
use Outbox\Client\Workfront\MimeTypes;

/**
 * Class WorkfrontClient
 * @package Outbox\Client\Workfront
 */
class WorkfrontClient
{

    /**
     * The default api path
     *
     * @var string
     */
    public static $default_api_path = '/attask/api';

    /**
     * Available API versions
     *
     * @var array
     */
    public static $available_versions = [
        0         => 'api-internal',
        5         => 'v5.0',
        6         => 'v6.0',
        7         => 'v7.0',
        8         => 'v8.0',
        9         => 'v9.0',
        10        => 'v10.0',
        11        => 'v11.0',
        'default' => 'v11.0'
    ];

    /**
     * The default base url to use if no url
     *
     * @var string
     */
    public static $default_base_url = 'https://api-cl01.attask-ondemand.com';
    protected $returnOnlyRequests = false;

    /**
     * The maximum number of records per request
     *
     * @var int
     */
    private $max_num_records = 2000;

    /**
     * The base url to use in requests
     *
     * @var string
     */
    private $base_url;
    /**
     * The version to use
     *
     * @var string
     */
    private $version;
    /**
     * The username to use for login
     *
     * @var string
     */
    private $username;
    /**
     * The password to use in login requests
     *
     * @var string
     */
    private $password;
    /**
     * The api key to use
     *
     * @var string
     */
    private $apiKey;
    /**
     * The HTTP Client to use to make requests
     *
     * @var HttpClient
     */
    private $http_client;
    /**
     * The last response made through the system
     *
     * @var
     */
    private $last_response;
    /**
     * The current login being used
     *
     * @var string
     */
    private $current_login;
    /**
     * The session ID for the current session ID
     *
     * @var string
     */
    private $session_id;

    /**
     * Sets up the client for use
     *
     * @param string|null $base_url
     * @param string|null $desired_version
     * @throws WorkfrontClientException
     */
    public function __construct($base_url = null, $desired_version = null)
    {
        $this->setBaseURL($base_url);
        $this->setVersion($desired_version);
    }

    /**
     * Getters
     * ====================================
     */

    /**
     * Get the last response
     *
     * @return mixed
     */
    public function getLastResponse()
    {
        return $this->last_response;
    }

    /**
     * Return the full url for the API
     *
     * @return string
     */
    public function getFullUrl()
    {
        return $this->base_url . $this->getApiPath();
    }

    /**
     * Get the path for the api
     *
     * @return string
     */
    public function getApiPath()
    {
        if ($this->version === 'api-internal') {
            return '/attask/' . $this->version;
        } else {
            return '/attask/api/' . $this->version;
        }
    }

    /**
     * Returns a string of the base url
     *
     * @return string The base url
     */
    public function getBaseUrl()
    {
        return $this->base_url;
    }

    /**
     * Returns the current workfront api version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Get the current login
     *
     * @return array
     */
    public function getCurrentLogin()
    {
        return $this->current_login;
    }

    /**
     * Get the current session ID
     *
     * @return string The Session ID
     */
    public function getSessionID()
    {
        return $this->session_id;
    }

    /**
     * @return HttpClient
     */
    public function getHttpClient()
    {
        return $this->http_client;
    }

    /**
     * Setters
     * ====================================
     */

    /**
     * Sets the base url to send requests to
     *
     * @param $base_url
     * @throws WorkfrontClientException
     */
    public function setBaseURL($base_url)
    {
        if ($base_url === null || $base_url === '') {
            $this->base_url = self::$default_base_url;
        } else {
            $hasPrefix = preg_match("/https?.*/", $base_url);
            if (!$hasPrefix) {
                $base_url = 'https://' . $base_url;
            } else if (strpos($base_url, 'http://') !== false) {
                $base_url = str_replace('http://', 'https://', $base_url);
            }
            $this->base_url = rtrim($base_url, '/');
        }

        $this->instantiateHttpClient();
    }

    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Set the HTTP Client
     *
     * @param HttpClient $client
     */
    public function setHttpClient(HttpClient $client)
    {
        $this->http_client = $client;
    }

    /**
     * Sets the api version to use
     *
     * @param $version
     * @throws WorkfrontClientException
     */
    public function setVersion($version)
    {
        if ($version === null || $version === '') {
            $this->version = self::$available_versions['default'];
        } else {
            if (!is_int($version)) {
                $version = intval(preg_replace("/[^0-9,.]/", '', $version));
            }

            if (isset(self::$available_versions[$version])) {
                $this->version = self::$available_versions[$version];
            } else {
                throw new WorkfrontClientException('There is not a version for the passed value');
            }
        }
    }

    public function setSessionID($session_id)
    {
        $this->session_id = $session_id;
        return $this;
    }

    public function returnOnlyRequests()
    {
        $this->returnOnlyRequests = true;
        return $this;
    }

    public function returnResponses()
    {
        $this->returnOnlyRequests = false;
        return $this;
    }

    /**
     * API Calls
     * ====================================
     */

    /**
     * Login as a workfront user
     *
     * @throws WorkfrontClientException
     * @param $username
     * @param $password
     * @return array The current login response
     */
    public function login($username, $password)
    {
        if (Support::isValidEmail($username)) {
            $this->username = $username;
            $this->password = $password;
        } else {
            throw new WorkfrontClientException('The passed username is not a valid email address');
        }

        $login_response = $this->postRequest('/login', ['username' => $this->username, 'password' => $this->password]);

        if($this->returnOnlyRequests === false) {
            $this->current_login = $login_response;
            $this->session_id = $login_response['sessionID'];
        }

        return $login_response;
    }

    /**
     * Impersonate a user
     *
     * This is an alias for the method loginAs
     *
     * @param $username
     * @return mixed
     * @throws WorkfrontClientException
     */
    public function impersonate($username)
    {
        return $this->loginAs($username);
    }

    /**
     * Login as a user
     *
     * @param $username
     * @return mixed
     * @throws WorkfrontClientException
     */
    public function loginAs($username)
    {
        if (isset($this->session_id) || isset($this->apiKey)) {
            $login_response = $this->postRequest('/login', ['username' => $username]);
            return $login_response;
        } else {
            throw new WorkfrontClientException('You are not logged in, so you can\'t log in as someone else');
        }
    }

    /**
     * Logout an user
     *
     * @return bool|mixed
     */
    public function logout()
    {
        if (isset($this->session_id)) {
            return $this->post('/logout', [
                'sessionID' => $this->session_id,
                'method'    => 'GET'
            ]);
        }
        return false;
    }

    /**
     * Gets the current user
     *
     * This is an alias for getLoggedInUser()
     *
     * @param null $return_fields
     * @return mixed
     */
    public function whoami($return_fields = null)
    {
        return $this->getLoggedInUser($return_fields);
    }

    /**
     * Gets the currently logged in user
     *
     * @param null $return_fields
     * @return mixed
     */
    public function getLoggedInUser($return_fields = null)
    {
        return $this->search('/user', ['ID' => '$$USER.ID', 'ID_Mod' => 'contains'], $return_fields)[0];
    }

    /**
     * Deletes the currently logged in user
     *
     * @param $object_code
     * @param $object_id
     * @param bool $force
     * @return mixed
     */
    public function delete($object_code, $object_id, $force = false)
    {
        $request_params = [
            'method' => 'DELETE',
            'force'  => $force
        ];
        return $this->postRequest('/' . $object_code . '/' . $object_id, $request_params);
    }

    //    public function bulkDelete($object_code, $ids, $force = false, $atomic = true)
    //    {
    //        $queue = [];
    //        foreach($ids as $id) {
    //            $queue[] = urlencode($object_code . '/' . $id . '?method=DELETE&force=' . var_export($force,true));
    //        }
    //
    //        $request = [
    //            'body' => 'atomic=' . var_export($atomic, true) . '&uri=' . implode('&uri=', $queue),
    //            'headers' => [
    //                'SessionID' => $this->session_id
    //            ]
    //        ];
    //
    ////        $request = $this->http_client->createRequest('POST', $this->getApiPath() . "/" . $object_code . "/bulk");
    ////        $query = $request->getBody();
    ////        $query->write('sessionID=' . $this->session_id . '&atomic=' . var_export($atomic, true) . '&uri=' . implode('&uri=', $queue));
    ////        $query['sessionID'] = $this->session_id;
    ////        $query['uri'] = $queue;
    ////        $query->setAggregator($query::duplicateAggregator());
    //
    ////        echo $request->getBody()->getContents();
    //        return $this->fireRequest(function() use ($request,$object_code, $id) {
    //            $response =  $this->http_client->post($this->getApiPath() . '/' . $object_code, $request);
    //            return $this->processResponse($response);
    //
    //        });
    //    }

    /**
     * Uploads a file to Workfront
     *
     * @param $filePath
     * @param array $newDocArray If an array is passed, this function will try to create an object for this file
     * @param string $objectCode
     * @param array $returnFields
     * @return mixed
     * @internal param array $return_fields
     */
    public function upload($filePath, array $newDocArray = [], $objectCode = 'document', $returnFields = [])
    {
        $bodyParams = [
            [
                'name' => 'uploadedFile',
                'contents' => fopen($filePath, 'r')
            ]
        ];

        if (empty($newDocArray)) {
            return $this->postRequest('/upload', $bodyParams, true);
        } else {
            $response = $this->postRequest('/upload', $bodyParams, true);
            $newDocArray['handle'] = $response['handle'];

            return $this->create($objectCode, $newDocArray, $returnFields);
        }

    }

    public function uploadAvatar($id, $filePath)
    {
        $response = $this->upload($filePath);

        $updates = [
            'handle' => $response['handle']
        ];

        return $this->update('avatar', $id, $updates);
    }

    /**
     * Downloads an user's avatar from Workfront
     *
     * @param string $id The ID of the workfront user
     * @param string $filename The filename of the jpg
     * @param mixed $location The location to store the files
     * @param mixed $versionId
     * @return array
     */
    public function downloadAvatar($id, $filename, $location = false, $versionId = false)
    {
        return $this->download($id, $filename, $location, '/user/avatar', $versionId);
    }

    /**
     * Downloads a file from Workfront
     *
     * @param string $id The ID of the document to download
     * @param string $filename The filename to store the download
     * @param mixed $location The location to store the file
     * @param string $urlPath
     * @param bool|string $versionID The version of the document to download
     * @return array The file path and file name of the downloaded file
     * @throws Exceptions\WorkfrontRequestException
     * @internal param string $urlObject
     * @internal param string $urlPath
     */
    public function download($id, $filename, $location = false, $urlPath = '/document/download', $versionID = false)
    {
        $request = [
            'query' => [
                'ID' => $id
            ]
        ];

        if ($versionID !== false) {
            $request['query']['versionID'] = $versionID;
        }

        if ($location === false) {
            $location = sys_get_temp_dir();
        }

        $this->setAuthHeader($request);

        return $this->fireRequest(function () use ($urlPath, $request, $filename, $location) {
            $response = $this->http_client->get($urlPath, $request);
            $fileParts = pathinfo($filename);
            if (!isset($fileParts['extension'])) {
                $returnedFileExt = MimeTypes::getInstance()->extFromMimeType($response->getHeader('content-type'));
                $filename = $filename . '.' . $returnedFileExt;
            }

            $location = rtrim($location, "/");

            $fullPath = $location . '/' . $filename;

            $file = file_put_contents($fullPath, $response->getBody());

            return [
                'file_size' => $file,
                'file_path' => $fullPath
            ];
        });
    }

    public function bulkAction($objCode, array $objects, $method)
    {
        $updates = json_encode($objects);
        return $this->postRequest('/' . $objCode, ['updates' => $updates], null, $method);
    }

    /**
     * Gets the API key for a users
     *
     * @param $username
     * @param $password
     * @return mixed
     * @throws WorkfrontClientException
     */
    public function getApiKey($username, $password)
    {
        if (isset($this->session_id)) {
            $api_key = $this->http_client->post('/attask/api-internal/user', [
                'headers' => [
                    'SessionID' => $this->session_id
                ],
                'body'    => ['action' => 'getApiKey', 'username' => $username, 'password' => $password, 'method' => 'put']
            ]);

            return $this->processResponse($api_key);
        } else {
            throw new WorkfrontClientException('You are not logged in, so you can\'t get any API keys');
        }

    }

    /**
     * Gets an object from the Workfront API
     *
     * @param string $object_code
     * @param string $object_id
     * @param string|array $fields
     * @return mixed
     */
    public function get($object_code, $object_id, $return_fields = null)
    {
        $request_array = ['method' => 'GET'];
        $this->prepareRequest($request_array, $return_fields);

        return $this->postRequest('/' . $object_code . '/' . $object_id, $request_array);
    }

    /**
     * Reports for a set of objects
     *
     * @param $object_code
     * @param array $query_params
     * @param null $return_fields
     * @param bool $all
     * @return mixed
     * @internal param array $query
     */
    public function report($object_code, array $query_params, $return_fields = null, $all = false)
    {
        $query_params = ["filters" => json_encode($query_params)];
        $request_array = array_merge($query_params, ['method' => 'GET']);
        $this->prepareRequest($request_array, $return_fields);

        if ($all === false) {
            return $this->postRequest('/' . $object_code . '/report', $request_array);
        }

        return $this->allObjects(function($request_params) use ($object_code) {
            return $this->postRequest('/' . $object_code . '/report', $request_params);
        }, $request_array);
    }

    /**
     * Search for a set of objects
     *
     * @param $object_code
     * @param $query_params
     * @param string|array $return_fields
     * @param bool $all
     * @return mixed
     */
    public function search($object_code, $filters, $return_fields = null, $query_params = [], $all = false)
    {
        $query_params['filters'] = json_encode($filters);
        $request_array = array_merge($query_params, ['method' => 'GET']);
        $this->prepareRequest($request_array, $return_fields);
        if ($all === false) {
            return $this->postRequest('/' . $object_code . '/search', $request_array);
        }

        return $this->allObjects(function($request_params) use ($object_code) {
            return $this->postRequest('/' . $object_code . '/search', $request_params);
        }, $request_array);
    }

    /**
     * Gets all objects in a search no matter the limit
     *
     * @param \Closure $query
     * @param array $request_array
     * @return array
     */
    private function allObjects(\Closure $query, array $request_array)
    {
        if(! isset($request_array['$$LIMIT'])) $request_array['$$LIMIT'] = $this->max_num_records;
        if( !isset($request_array['$$FIRST'])) $request_array['$$FIRST'] = 0;

        $objects = $query($request_array);
        $count = count($objects);

        while($count === $request_array['$$LIMIT']) {
            $request_array['$$FIRST'] = count($objects);
            $newObjects = $query($request_array);
            $count = count($newObjects);
            $objects = array_merge($objects,$newObjects);
        }

        return $objects;
    }

    /**
     * Retrieves API metadata for an object.
     *
     * @throws WorkfrontClientException
     * @param  {string|null} $objCode [optional]
     * @return object
     */
    public function metadata($objCode = null)
    {
        // Build request path
        $path = '';
        if (!empty($objCode)) {
            $path .= '/' . $objCode;
        }

        //return $this->request($path, null, null, self::METH_GET);
        return $this->postRequest($path . '/metadata', []);
    }

    /**
     * Get the count of a query in Workfront
     *
     * @param $object_code
     * @param $query_params
     * @param null $return_fields
     * @return mixed
     */
    public function count($object_code, $query_params, $return_fields = null)
    {
        $request_array = array_merge($query_params, ['method' => 'GET']);
        $this->prepareRequest($request_array, $return_fields);

        return $this->postRequest('/' . $object_code . '/count', $request_array)['count'];
    }

    /**
     * Update an object
     *
     * @param $object_code
     * @param $object_id
     * @param array $updates
     * @param string|array $return_fields
     * @return mixed
     */
    public function update($object_code, $object_id, array $updates, $return_fields = null)
    {
        return $this->put($object_code, $object_id, $updates, $return_fields);
    }

    /**
     * Perform a PUT action on an object
     *
     * @param $object_code
     * @param $object_id
     * @param array $updates
     * @param string|array $return_fields
     * @return mixed
     */
    public function put($object_code, $object_id, array $updates, $return_fields = null)
    {
        if(!empty($updates)) {
            $request_array = [
                'method'  => 'PUT',
                'updates' => json_encode($updates)
            ];

            $this->prepareRequest($request_array, $return_fields);

            return $this->postRequest('/' . $object_code . '/' . $object_id, $request_array);
        } else {
            return ['ID' => $object_id];
        }

    }

    /**
     * Run a named query
     * @param $object_code
     * @param $queryName
     * @param array $query
     * @param null $return_fields
     * @return mixed
     * @throws Exceptions\WorkfrontRequestException
     */
    public function namedQuery($object_code, $queryName, array $query, $return_fields = null)
    {
        $request_array = array_merge($query, ['method' => 'GET']);
        $this->prepareRequest($request_array, $return_fields);
        return $this->postRequest("/" . $object_code . "/" . $queryName, $request_array);
    }

    /**
     * Call an Action for a Workfront Object
     *
     * @param $object_code
     * @param $object_id
     * @param $action
     * @param null $arguments
     * @return mixed
     */
    public function action($object_code, $object_id, $action, $arguments = null)
    {
        $path = '/' . $object_code . '/' . $object_id . '/' . $action;
        $params = [
            'method' => 'PUT'
        ];

        if(isset($arguments)) {
            $params['updates'] = json_encode($arguments);
        }

        return $this->postRequest($path, $params);
    }

    /**
     * Create a new object
     *
     * @param $object_code
     * @param array $params
     * @param string|array $return_fields
     * @return mixed
     */
    public function create($object_code, array $params, $return_fields = null)
    {
        return $this->post($object_code, $params, $return_fields);
    }

    /**
     * Perform a post request on an object
     *
     * @param $object_code
     * @param array $params
     * @param string|array $return_fields
     * @return mixed
     */
    public function post($object_code, array $params, $return_fields = null)
    {
        $request_array = [
            'updates' => json_encode($params)
        ];

        $this->prepareRequest($request_array, $return_fields);

        return $this->postRequest('/' . $object_code, $request_array);
    }

    /**
     * Create objects with a bulk request
     *
     * @param $object_code
     * @param array $objects
     * @param null $return_fields
     * @return array
     */
    public function bulkCreate($object_code, array $objects, $return_fields = null)
    {
        $return_array = [];

        $request_array = [
            'updates' => json_encode($objects),
        ];

        $this->prepareRequest($request_array, $return_fields);

        $return_array = $this->postRequest('/' . $object_code, $request_array);

        // TODO: Implement automatic chunking and requests
        //        $object_chunks = array_chunk($objects, 100, false);
        //
        //        foreach ($object_chunks as $chunk) {
        //            $request_array = [
        //                'updates' => json_encode($chunk),
        //            ];
        //
        //            $this->prepareRequest($request_array, $return_fields);
        //
        //            $return_array = array_merge($return_array, $this->postRequest('/' . $object_code, $request_array));
        //        }

        return $return_array;
    }

    /**
     * Update objects with a bulk request
     *
     * @param $object_code
     * @param array $objects
     * @param null $return_fields
     * @return array
     */
    public function bulkUpdate($object_code, array $objects, $return_fields = null)
    {
        $return_array = [];

        // TODO: Implement automatic chunking and requests
        $request_array = [
            'updates' => json_encode($objects),
            'method'  => 'PUT'
        ];

        $this->prepareRequest($request_array, $return_fields);

        $return_array = $this->postRequest('/' . $object_code, $request_array);

        //        $object_chunks = array_chunk($objects, 100, false);
        //
        //        foreach ($object_chunks as $chunk) {
        //            $request_array = [
        //                'updates' => json_encode($chunk),
        //                'method'  => 'PUT'
        //            ];
        //
        //            $this->prepareRequest($request_array, $return_fields);
        //
        //            $return_array = array_merge($return_array, $this->postRequest('/' . $object_code, $request_array));
        //        }

        return $return_array;
    }

    public function setAuthHeader(&$request)
    {
        if(!isset($request['headers'])) {
            $request['headers'] = [];
        }
        if(!$this->setSessionHeader($request)) {
            $this->setApiHeader($request);
        }
    }

    /**
     * @param $request
     */
    public function setSessionHeader(&$request)
    {
        if(!isset($request['headers'])) {
            $request['headers'] = [];
        }
        if (isset($this->session_id)) {
            $request['headers']['SessionID'] = $this->session_id;
            return true;
        }

        return false;
    }

    public function setApiHeader(&$request)
    {
        if(!isset($request['headers'])) {
            $request['headers'] = [];
        }
        if (isset($this->apiKey) && $this->apiKey != null && !isset($this->session_id)) {
            $request['headers']['ApiKey'] = $this->apiKey;
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function isReturnOnlyRequests()
    {
        return $this->returnOnlyRequests;
    }



    /**
     * Private Functions
     * ====================================
     */

    /**
     * Setup the HttpClient
     *
     * @return void
     */
    private function instantiateHttpClient()
    {
        $this->http_client = new HttpClient([
            'base_uri' => $this->base_url
        ]);
    }

    /**
     * Processes the response from the HTTP Client
     *
     * @param Response $response
     * @return mixed
     */
    public function processResponse(Response $response)
    {
        $this->last_response = $response;
        try {
            $response_json = json_decode($response->getBody(), true);
        } catch(Exception $e) {
            if($e->getResponse()->getStatusCode() === 200) {
                return [
                    'response' => 'success',
                    'message' => 'Unable to parse JSON, but the query was successful'
                ];
            }

            throw $e;
        }

        if (isset($response_json['data'])) {
            return $response_json['data'];
        } else {
            return $response_json;
        }
    }

    /**
     * Prepare the request array for the request
     *
     * @param array $request_params
     * @param string|array $return_fields
     */
    private function prepareRequest(array &$request_params, $return_fields)
    {
        if (isset($return_fields)) {
            if (is_array($return_fields)) {
                $request_params['fields'] = json_encode($return_fields);
            } else {
                $request_params['fields'] = $return_fields;
            }
        }
    }

    /**
     * Makes an authorized post request
     *
     * @param string $path
     * @param array $body_params
     * @param bool $isUpload
     * @return mixed
     */
    private function postRequest($path, array $body_params, $isUpload = false)
    {
        if(!$isUpload) {
            $requestBody = [
                'form_params' => $body_params,
            ];
        } else {
            $requestBody = [
                'multipart' => $body_params,
            ];
        }

        $this->setAuthHeader($requestBody);

        return $this->fireRequest(function () use ($path, $requestBody) {
            $request = new Request('POST',$this->getApiPath() . $path);

            if($this->returnOnlyRequests === false) {
                return $this->processResponse($this->http_client->send($request, $requestBody));
            }
            //            $response = $this->http_client->post($this->getApiPath() . $path, $request);
            return $request;
        });
    }

    private function fireRequest(callable $closure)
    {
        try {
            return $closure();
        } catch (BadResponseException $e) {
            $workfont_client_exception = WorkfrontHttpExceptionFactory::create($e);
            throw $workfont_client_exception;
        }
    }
}
