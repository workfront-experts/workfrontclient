<?php namespace Outbox\Client\Workfront\Exceptions;


use GuzzleHttp\Exception\BadResponseException;

class WorkfrontRequestException extends BaseHttpException {

    public function __construct(BadResponseException $e){
        parent::__construct($e);
    }

}