<?php namespace Outbox\Client\Workfront\Exceptions;


use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class WorkfrontHttpExceptionFactory {
    static function create(BadResponseException $exception) {
        $response = json_decode($exception->getResponse()->getBody(), true);
        return new WorkfrontRequestException($exception);
    }
}