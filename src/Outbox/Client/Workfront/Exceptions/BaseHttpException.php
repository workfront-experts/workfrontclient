<?php namespace Outbox\Client\Workfront\Exceptions;

class BaseHttpException extends \Exception{

    public function __construct(\GuzzleHttp\Exception\BadResponseException $exception){

        try {
            $response = json_decode($exception->getResponse()->getBody(), true);
            if(isset($response['error'])) {
                parent::__construct($response['error']['message']);
            } else {
                parent::__construct($exception->getMessagE());
            }

            $this->code = $exception->getCode();
            $this->request = $exception->getRequest();
            $this->response = $exception->getResponse();
            $this->file = $this->getFile();
        } catch(\GuzzleHttp\Exception\ParseException $e) {
            parent::__construct($exception->getMessage());

            $this->code = $exception->getCode();
            $this->request = $exception->getRequest();
            $this->response = $exception->getResponse();
            $this->file = $this->getFile();
        } catch(\Exception $e) {
            throw $e;
        }
    }
}