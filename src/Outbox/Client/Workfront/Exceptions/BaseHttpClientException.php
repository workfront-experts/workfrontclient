<?php namespace Outbox\Client\Workfront\Exceptions;


use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ParseException;

abstract class BaseHttpClientException extends \Exception {

    private $response;
    private $request;

    public function __construct(ClientException $exception)
    {
        try {
            $response = $exception->getResponse()->json();
            if(isset($response['error'])) {
                parent::__construct($response['error']['message']);
            } else {
                parent::__construct($exception->getMessagE());
            }

            $this->code = $exception->getCode();
            $this->request = $exception->getRequest();
            $this->response = $exception->getResponse();
            $this->file = $this->getFile();
        } catch(ParseException $e) {
            parent::__construct($exception->getMessage());

            $this->code = $exception->getCode();
            $this->request = $exception->getRequest();
            $this->response = $exception->getResponse();
            $this->file = $this->getFile();
        } catch(\Exception $e) {
            throw $e;
        }

    }
}