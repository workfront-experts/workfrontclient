<?php namespace Outbox\Client\Workfront;
    /*
     * Copyright (c) 2010 AtTask, Inc.
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */
// API Version
set_time_limit(0);
if(!defined('API_V1')){
    define('API_V1','/attask/api/v1.0');
}
if(!defined('API_V2')){
    define('API_V2','/attask/api/v2.0');
}
if(!defined('API_V3')){
    define('API_V3','/attask/api/v3.0');
}
if(!defined('API_INTERNAL')){
    define('API_INTERNAL','/attask/api-internal');
}
if(!defined('HTTP')){
    define('HTTP','https://');
}
if(!defined('TEMP')){
    define('TEMP','/var/chroot/home/content/39/7649239/html/tmp/');
}

/**
 * StreamClient class.
 *
 * @throws WorkfrontClientException
 * @package StreamRestClient
 */
class WorkfrontClientOld
{

    // Supported request methods
    const
        METH_DELETE = 'DELETE',
        METH_GET    = 'GET',
        METH_POST   = 'POST',
        METH_PUT    = 'PUT';

    const
        API_V1 = "v1.0",
        API_V2 = "v2.0",
        API_V3 = "/attask/api/v3.0",
        API_INTERNAL = "/attask/api-internal";

    // Well known paths
    const
        PATH_LOGIN  = '/login',
        PATH_LOGOUT = '/logout',
        PATH_SEARCH = '/search',
        PATH_COUNT  = '/count',
        PATH_BATCH  = '/batch',
        PATH_METADATA = '/metadata',
        PATH_REPORT = '/report',

        //Screenscrap Objects
        PATH_ATTASK = '/attask',
        PATH_UIVIEW = 'uiview',

        //Screenscrap Actions
        PATH_DELETE = 'Delete.cmd', //Delete Mode
        PATH_OPEN = 'Open.cmd', //Edit Mode
        PATH_EDIT = 'Edit.cmd', //Put Mode
        PATH_ADD = 'Add.cmd', //Post Mode
        PATH_VIEW = 'View.cmd'; //View Mode

    public
        $handle = null,
        $hostname = null,
        $domain = null,
        $sessionID = null,
        $debug = null,
        $apiKey = null;

    /**
     * Creates an instance of the client.
     *
     * @param  string $hostname
     * @return void
     */
    public function __construct ($hostname, $version = 'v3.0') {

        if(strpos($hostname, 'https://') !== false)
        {
            $hostname = str_replace('https://', '', $hostname);
            $hostname = str_replace('/attask', '', $hostname);
        }

        $this->domain = $hostname; //Just store the domain information for use outside the API context

        if($version == 'v1.0')
        {
            $this->hostname = HTTP . $hostname . API_V1; // Store the full URI for the API
        }elseif($version == 'v2.0')
        {
            $this->hostname = HTTP . $hostname . API_V2; // Store the full URI for the API
        }elseif($version == 'v3.0')
        {
            $this->hostname = HTTP . $hostname . API_V3; // Store the full URI for the API
        }elseif($version == 'internal')
        {
            $this->hostname = HTTP . $hostname . API_INTERNAL; // Store the full URI for the API
        }else
        {
            $this->hostname = HTTP . $hostname . '/attask/api'; // Most current version
        }
        // Initialize cURL
        if (is_null($this->handle)) {
            $this->handle = curl_init();
            curl_setopt($this->handle, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($this->handle, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($this->handle, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($this->handle, CURLOPT_TIMEOUT, 240);
            curl_setopt($this->handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($this->handle, CURLOPT_POST, true);
            curl_setopt($this->handle, CURLOPT_BUFFERSIZE, 128000);
        }
    }

    /**
     * Destroys an instance of the client.
     *
     * @return void
     */
    public function __destruct () {
        // Close cURL
        if (!is_null($this->handle)) {
            curl_close($this->handle);
            $this->handle = null;
        }
    }

    /**
     * Login to @task
     *
     * @throws WorkfrontClientException
     * @param  string $username
     * @param  string $password
     * @return object
     */
    public function login ($username, $password) {
        $this->username = $username;
        $this->password = $password;
        return $this->request(self::PATH_LOGIN, array('username' => $username, 'password' => $password), null, self::METH_GET);
    }

    public function loginAs ($username) {
        if(isset($this->sessionID)) {
            return $this->request(self::PATH_LOGIN, array('username' => $username), null, self::METH_POST);
        } else {
            throw new WorkfrontClientException('You are not logged in, so you can\'t log in as someone else');
        }
    }

    public function getApiKey($username, $password){
        $result = $this->request('/user', array('action' => "getApiKey", 'username' => $username, 'password' => $password), null, self::METH_PUT);
        return $result;
    }

    /**
     * Logout from @task
     *
     * @throws WorkfrontClientException
     * @return bool
     */
    public function logout () {
        $result = $this->request(self::PATH_LOGOUT, array('sessionID' => $this->sessionID), null, self::METH_GET);
        return $result->success;
    }

    ////////////////////////////////////////////////
    // Details about the current user
    ////////////////////////////////////////////////
    public function whoami($fields = '')
    {
        return $this->search('user', array('ID'=>'$$USER.ID', 'ID_Mod'=>'contains'), $fields);
    }

    ////////////////////////////////////////////////
    // Create Object outside of the API
    ////////////////////////////////////////////////
    public function atAdd($objCode, $params = '')
    {
        return $this->atRequest(self::PATH_ATTASK . '/' . $objCode . self::PATH_ADD, $params);
    }

    private function atRequest ($path, $params) {
        $query = 'secure_code=' . $this->sessionID;
        $query .= '&sessionID=' . $this->sessionID;
        //$query = 'username=' . $this->username;
        //$query .= '&password=' . $this->password;


        $cookie = 'sessionID=' . $this->sessionID . ';path=/attask';

        if (!is_null($params)) {
            $query .= '&' . http_build_query($params);
        }

        // Set dynamic cURL options
        curl_setopt($this->handle, CURLOPT_URL, HTTP . $this->domain . $path);
        curl_setopt($this->handle, CURLOPT_COOKIE, $cookie);
        curl_setopt($this->handle, CURLOPT_COOKIESESSION, TRUE);
        curl_setopt($this->handle, CURLOPT_COOKIEFILE, TEMP . "cookiefile");
        curl_setopt($this->handle, CURLOPT_COOKIEJAR, TEMP . "cookiefile");
        curl_setopt($this->handle, CURLOPT_POSTFIELDS, urlEncode($query));
        //if($this->debug)
        //{
        echo "----Call----" . HTTP . $this->domain . $path . '?' . $query . PHP_EOL;
        //}

        // Execute request
        if (!($response = curl_exec($this->handle)))
        {
            print_r(curl_getinfo($this->handle));
            //throw new WorkfrontClientException(curl_error($this->handle));
        }
        var_dump($response);

        exit;
        //$result = json_decode($response);

        // Verify result
        if (isset($result->error)) {
            throw new WorkfrontClientException($result->error->message);
            //print "ERROR " . $result->error->message;
        }
        else if (!isset($result->data))
        {
            $result = json_decode($response);
            // Verify result
            if (isset($result->error)) {
                throw new WorkfrontClientException($result->error->message);
            }
            else if (!isset($result->data)) {
                throw new WorkfrontClientException('Invalid response from server');
            }
        }

        if($this->debug)
        {
            echo '----Results----<br>';
            print_r($result->data);
            echo "-------------------------------------------------". PHP_EOL;
        }
        return objectToArray($result->data);
    }


    //END ATREQUESTS


    /**
     * Searches for all objects that match a given query.
     *
     * @throws WorkfrontClientException
     * @param  string $objCode
     * @param  array $query
     * @param  array $fields [optional]
     * @return array
     */
    public function search ($objCode, $query, $fields = null) {

        if(isset($query['$$FIRST']) || isset($query['$$LIMIT']))
        {
            // Respect them if they are set.
            return $this->request('/'.$objCode.self::PATH_SEARCH, (array) $query, $fields, self::METH_GET);
        }

        $count = $this->atcount($objCode, $query);

        if($count['count'] > 100 && $count['count'] < 2000)
        {
            $query['$$FIRST'] = '0';
            $query['$$LIMIT'] = '2000';
            return $this->request('/'.$objCode.self::PATH_SEARCH, (array) $query, $fields, self::METH_GET);
        }elseif($count['count'] > 2000)
        {
            $loop = $count['count'] / 2000;

            $query['$$FIRST'] = '0';
            $query['$$LIMIT'] = '2000';
            $results = array();
            for($i = 0; $i < $loop; $i++)
            {
                $results = array_merge_recursive($this->request('/'.$objCode.self::PATH_SEARCH, (array) $query, $fields, self::METH_GET), $results);
                $query['$$FIRST'] = $query['$$FIRST'] + 2000;
                //print $i . ' ' . $query['$$FIRST'] . ' ' . $count['count'] . ' ' . $loop . PHP_EOL;
            }
            return $results;

        }else
        {
            // Just the small non-limited stuff
            return $this->request('/'.$objCode.self::PATH_SEARCH, (array) $query, $fields, self::METH_GET);
        }
    }

    /**
     * Reports for all objects that match a given query.
     *
     * @throws WorkfrontClientException
     * @param  string $objCode
     * @param  array $query
     * @param  array $agg
     * @return array
     */
    public function report ($objCode, $query) {

        if(isset($query['$$FIRST']) || isset($query['$$LIMIT']))
        {
            // Respect them if they are set.
            return $this->request('/'.$objCode.self::PATH_REPORT, (array) $query, $fields, self::METH_GET);
        }

        $count = $this->atcount($objCode, $query);

        if($count['count'] > 100 && $count['count'] < 2000)
        {
            $query['$$FIRST'] = '0';
            $query['$$LIMIT'] = '2000';
            return $this->request('/'.$objCode.self::PATH_REPORT, (array) $query, $fields, self::METH_GET);
        }elseif($count['count'] > 2000)
        {
            $loop = $count['count'] / 2000;

            $query['$$FIRST'] = '0';
            $query['$$LIMIT'] = '2000';
            $results = array();
            for($i = 0; $i < $loop; $i++)
            {
                $results = array_merge_recursive($this->request('/'.$objCode.self::PATH_REPORT, (array) $query, $fields, self::METH_GET), $results);
                $query['$$FIRST'] = $query['$$FIRST'] + 2000;
            }
            return $results;

        }else
        {
            // Just the small non-limited stuff
            return $this->request('/'.$objCode.self::PATH_REPORT, (array) $query, $fields, self::METH_GET);
        }
    }

    /**
     * Counts total that would be returned for a given query.
     *
     * @throws WorkfrontClientException
     * @param  string $objCode
     * @param  array $query
     * @return array
     */
    public function atcount ($objCode, $query, $fields = null) {
        return $this->request('/'.$objCode.self::PATH_COUNT, (array) $query, $fields, self::METH_GET);
    }

    /**
     * Named Queries total that would be returned for a given query.
     *
     * @throws WorkfrontClientException
     * @param  string $objCode
     * @param  string $path
     * @param  array $query
     * @return array
     */
    public function namedquery ($objCode, $path, $query, $fields = null) {
        return $this->request('/' . $objCode . '/' . $path, (array) $query, $fields, self::METH_GET);
    }

    /**
     * Batch Queries total that would be returned for a given query.
     *
     * @throws WorkfrontClientException
     * @param  array $query
     * @return array
     */
    public function batch ($query) {
        return $this->request(self::PATH_BATCH, (array) $query, null, self::METH_GET);
    }




    /**
     * Retrieves an object by ID.
     *
     * @throws WorkfrontClientException
     * @param  string $objCode
     * @param  string $objID
     * @param  array $fields [optional]
     * @return object
     */
    public function get ($objCode, $objID, $fields = null) {
        $result = $this->request('/'.$objCode.'/'.$objID, null, $fields, self::METH_GET);
        return $result;
    }

    /**
     * Retrieves an object by ID.
     *
     * @throws WorkfrontClientException
     * @param  string $objCode
     * @param  string $objID
     * @param  array $fields [optional]
     * @return object
     */
    public function action ($objCode, $objID, $fields = null) {

        if($fields == 'move')
        {
            $method = self::METH_POST;
        }else
        {
            $method = self::METH_PUT;
        }

        $result = $this->request('/'.$objCode.'/'.$objID. '/'. $fields, null, null, $method);
        return $result;
    }

    /**
     * Inserts a new object.
     *
     * @throws WorkfrontClientException
     * @param  string $objCode
     * @param  array $message
     * @param  array $fields [optional]
     * @return object
     */
    public function post ($objCode, $message, $fields = null) {
        return $this->request('/'.$objCode, (array) $message, $fields, self::METH_POST);
    }

    /**
     * Edits an existing object.
     *
     * @throws WorkfrontClientException
     * @param  string $objCode
     * @param  string $objID
     * @param  array $message
     * @param  array $fields [optional]
     * @return object
     */
    public function put ($objCode, $objID, $message, $fields = null) {
        return $this->request('/'.$objCode.'/'.$objID, array('updates' => json_encode($message)), $fields, self::METH_PUT);
    }

    /**
     * Sets audit flags on an existing object.
     *
     * @throws WorkfrontClientException
     * @param  string $objCode
     * @param  string $objID
     * @param  array $fields [optional]
     * @return object
     */
    public function audit ($objCode, $objID, $message = null, $fields = null) {

        if($objCode == 'portfolio' || $objCode == 'program' || $objCode == 'project' || $objCode == 'task' || $objCode == 'issue')
        {
            $audit = true;
        }

        if($objCode && $objID && $audit)
        {
            $message = '{"auditTypes": ["AA", "GE", "ST", "SC" ]}';
            return $this->request('/'.$objCode.'/'.$objID, array('updates' => $message), null, self::METH_PUT);
        }else
        {
            return false;
        }


    }


    /**
     * Deletes an object.
     *
     * @throws WorkfrontClientException
     * @param  string $objCode
     * @param  string $objID
     * @param  bool $force [optional]
     * @return bool
     */
    public function delete ($objCode, $objID, $force = false) {
        $result = $this->request('/'.$objCode.'/'.$objID, array('force' => $force), null, self::METH_DELETE);
        return $result->success;
    }

    /**
     * Retrieves API metadata for an object.
     *
     * @throws WorkfrontClientException
     * @param  {string|null} $objCode [optional]
     * @return object
     */
    public function metadata ($objCode = null) {
        // Build request path
        $path = '';
        if (!empty($objCode))
        {
            $path .= '/' . $objCode;
        }
        $path .= self::PATH_METADATA;

        //return $this->request($path, null, null, self::METH_GET);
        return $this->request($path, null, null, self::METH_GET);
    }
    /**
     * Uploads a document to an object.
     * Same as <input type="file" name="file_box">
     * $file = '@/var/chroot/home/content/39/7649239/html/tmp/MVC-002X.JPG'
     * @param  array $message
     * @param  array $fields [optional]
     * @throws WorkfrontClientException
     * @param  {string|null} $objCode [optional]
     * @return object
     */
    public function upload ($file, $message, $fields = null) {
        // Build request path
        $path = '/upload?sessionID=' . $this->sessionID;
        // Set dynamic cURL options
        curl_setopt($this->handle, CURLOPT_URL, $this->hostname . $path);
        curl_setopt($this->handle, CURLOPT_POSTFIELDS, array('uploadedFile' => $file));

        if($this->debug)
        {
            echo "----Call----" . $this->hostname . $path . PHP_EOL;
        }

        // Execute request
        if (!($response = curl_exec($this->handle))) {
            throw new WorkfrontClientException(curl_error($this->handle));
        }
        $result = json_decode($response);

        // Verify result
        if (isset($result->error)) {
            throw new WorkfrontClientException($result->error->message);
        }
        else if (!isset($result->data)) {
            //We are going to try to do the query again...
            if (!($response = curl_exec($this->handle))) {
                throw new WorkfrontClientException(curl_error($this->handle));
            }
            $result = json_decode($response);
            // Verify result
            if (isset($result->error)) {
                throw new WorkfrontClientException($result->error->message);
            }
            else if (!isset($result->data)) {
                //We are going to try to do the query again...
                if (!($response = curl_exec($this->handle))) {
                    throw new WorkfrontClientException(curl_error($this->handle));
                }
                $result = json_decode($response);
                // Verify result
                if (isset($result->error)) {
                    throw new WorkfrontClientException($result->error->message);
                }
                else if (!isset($result->data)) {
                    throw new WorkfrontClientException('Invalid response from server');
                }
            }
        }

        if($this->debug)
        {
            echo '----Results Upload----<br>';
            print_r($result->data);
            echo "-------------------------------------------------". PHP_EOL;
        }
        $h = $this->objectToArray($result->data);
        $message['handle'] = $h['handle'];
        return $this->request('/document', (array) $message, $fields, self::METH_POST);
    }

    /**
     * Downloads a file and stores it in the temp folder.
     * $param  string $url = '/attask/documentDownload.cmd?val(documentID)=2d2cac9004bf11e09e5600163e7aaf49'
     * @param  string $name [optional]
     * @throws WorkfrontClientException
     * @return fileHandle
     */

    public function download ($url, $name = 'File') {
        // Build request path
        $path = $url . '&sessionID=' . $this->sessionID;

        $fp = fopen ( TEMP . $name, 'w+');
        // Set dynamic cURL options
        curl_setopt($this->handle, CURLOPT_URL, HTTP . $this->domain . $path);
        curl_setopt($this->handle, CURLOPT_FILE, $fp);

        if($this->debug)
        {
            echo "----Call----" . HTTP . $this->domain . $path . PHP_EOL;
        }

        // Execute request
        if (!($response = curl_exec($this->handle))) {
            throw new WorkfrontClientException(curl_error($this->handle));
        }

        fflush($fp);
        return $fp;
    }


    /**
     * Converts objects returned into arrays.
     * This is necessary when returning complex objects.
     * For example, an object returned from a search using a cross-object reference cannot be displayed using methods to display simple objects...
     *   /api/task/search?fields=project:name
     *   /api/task/search?fields=DE:Parameter Name
     * both contain colons, which will result in a stdClass error when using the methods to reference simple objects.
     * The function below provides a way to convert the 'project:name' object into a usuable array
     *   i.e. $task['project:name'] can be used by placing the returned object into the function
     *
     */

    function objectToArray( $object )
    {
        if( !is_object( $object ) && !is_array( $object ) )
        {
            return $object;
        }
        if( is_object( $object ) )
        {
            $object = get_object_vars( $object );
        }
        return array_map( array($this, 'objectToArray'), $object );
    }

    /**
     * Converts ISODATE to unix date
     * 1984-09-01T14:21:31Z
     * i.e. $plannedStartDate = tstamptotime($task['plannedStartDate']);
     *
     */

    function tstamptotime($tstamp) {
        sscanf($tstamp,"%u-%u-%uT%u:%u:%uZ",$year,$month,$day,
            $hour,$min,$sec);
        $newtstamp=mktime($hour,$min,$sec,$month,$day,$year);
        return $newtstamp;
    }
    function erase_val(&$myarr) {
        $myarr = array_map(create_function('$n', 'return null;'), $myarr);
    }



    /**
     * Performs the request to the server.
     *
     * @throws WorkfrontClientException
     * @param  string $path
     * @param  array $params
     * @param  array $fields [optional]
     * @param  string $method
     * @return mixed
     */
    private function request ($path, $params = array(), $fields = null, $method) {

        $query = '';
        if(isset($this->sessionID) && $this->sessionID != null ){
            $params['sessionID'] = $this->sessionID;
        }
        else if(isset($this->apiKey) && $this->apiKey != null){
            $params['apiKey'] = $this->apiKey;
        }

        $params['method'] = $method;

        $needAmp = false;

        if (!is_null($params)) {
            $query = http_build_query($params);
            $needAmp = true;
        }

        if (!is_null($fields) && (is_array($fields) || is_object($fields))) {

            if($needAmp){
                $query .= "&";
            }

            $query .= 'fields=' . urlencode(implode(',', (array) $fields));
        }

        // Set dynamic cURL options
        curl_setopt($this->handle, CURLOPT_URL, $this->hostname . $path);// . '?' . $query);
        curl_setopt($this->handle, CURLOPT_POSTFIELDS, $query);
        if($this->debug)
        {
            echo "----Call----" . $this->hostname . $path . '?' . urlDecode($query) . PHP_EOL;
        }
//
//        echo "URL: " . $this->hostname . $path;
//        echo "Query: " . $query;
//        exit(0);

        // Execute request
        if (!($response = curl_exec($this->handle))) {
            //print_r(curl_getinfo($this->handle));
            throw new WorkfrontClientException(curl_error($this->handle));
        }
        //var_dump($response);
        $result = json_decode($response);

        // Verify result
        if (isset($result->error)) {
            throw new WorkfrontClientException($result->error->message);
            //print "ERROR " . $result->error->message;
        }
        else if (!isset($result->data)) {
            //We are going to try to do the query again...
            //sleep(1);
            //print 'Trying it again';
            if (!($response = curl_exec($this->handle))) {
                throw new WorkfrontClientException(curl_error($this->handle));
            }
            $result = json_decode($response);
            // Verify result
            if (isset($result->error)) {
                throw new WorkfrontClientException($result->error->message);
            }
            else if (!isset($result->data)) {
                //We are going to try to do the query again...
                //sleep(1);
                //print 'Trying it again';
                if (!($response = curl_exec($this->handle))) {
                    throw new WorkfrontClientException(curl_error($this->handle));
                }
                $result = json_decode($response);
                // Verify result
                if (isset($result->error)) {
                    throw new WorkfrontClientException($result->error->message);
                }
                else if (!isset($result->data)) {
                    throw new WorkfrontClientException('Invalid response from server');
                }
            }
        }

        // Manage the session
        if ($path == self::PATH_LOGIN) {
            $this->sessionID = $result->data->sessionID;
        }
        else if ($path == self::PATH_LOGOUT) {
            $this->sessionID = null;
        }

        if($this->debug)
        {
            echo '----Results----<br>';
            print_r($result->data);
            echo "-------------------------------------------------". PHP_EOL;
        }
        $t = $this->objectToArray($result->data);
        if(isset($t['null'])) //Fix an error that retuns a null array at the top
        {
            return $t['null'];
        }else
        {
            return $t;
        }
    }

//END of Class
}
