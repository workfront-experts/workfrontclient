<?php namespace Outbox\Client\Workfront;

use Outbox\Client\Workfront\Exceptions\ImpersonateException;
use Outbox\Client\Workfront\Exceptions\WorkfrontClientException;
use Outbox\Client\Workfront\Exceptions\WorkfrontRequestException;

class Impersonate {

    static private $instance = null;
    protected $current_login;

    private $master_session_id;
    /**
     * @var WorkfrontClient
     */
    private $client;
    private $saved_logins = [];
    private $id_to_email_maps = [];

    public function ___construct()
    {

    }

    static public function getInstance()
    {
        if(self::$instance === null) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    static public function withClient(WorkfrontClient $client)
    {
        self::getInstance()->setClient($client);
    }

    public function setClient(WorkfrontClient $client)
    {
        // TODO If the client isn't already logged in
        $this->client = $client;
        $this->master_session_id = $client->getSessionID();
    }

    /**
     * @return WorkfrontClient
     */
    public function getClient()
    {
        return $this->client;
    }

    static public function asUser($email)
    {
        return self::getInstance()->runAsUser($email);
    }

    public function runAsUser($emailOrId)
    {
        $email = $this->getEmailFromId($emailOrId);

        // Check if email is valid
//        if(Support::isValidEmail($email)) {
            // Check if email exists current logins
            if(!isset($this->saved_logins[$email])) {
                $return = false;
                if($this->client->isReturnOnlyRequests()) {
                    $this->client->returnResponses();
                    $return = true;
                }
                $this->addLogin($email, $this->client->loginAs($email));
                if($return) {
                    $this->client->returnOnlyRequests();
                }
            }
            $this->setCurrentLogin($this->saved_logins[$email]);

            return $this;
//        } else {
//            throw new ImpersonateException('The passed email is not a valid email');
//        }
    }

    /**
     * Add a login to the saved logins member parameter
     *
     * @param string $email
     * @param array $login
     */
    public function addLogin($email, $login)
    {
        // If it doesn't exist, login as and store in user array by email
        $this->saved_logins[$email] = $login;
        $this->id_to_email_maps[$login['userID']] = $email;
    }

    public function resetLogins()
    {
        unset($this->saved_logins);
        unset($this->id_to_email_maps);
        $this->saved_logins = [];
        $this->id_to_email_maps = [];
    }

    public function refreshLogins()
    {
//        $newSavedLogins = [];
        foreach($this->saved_logins as $email => $login) {
            $this->addLogin($email,$this->client->loginAs($email));
        }
//        $this->saved_logins = $newSavedLogins;
    }

    public function __call($name, $args)
    {
        if(method_exists($this->client, $name)) {
            if(isset($this->current_login)) {
                try {
                    $master_session_id = $this->client->getSessionID();
                    $this->client->setSessionID($this->current_login['sessionID']);
                    $response = call_user_func_array([$this->client, $name], $args);
                    $this->client->setSessionID($master_session_id);
                    unset($this->current_login);
                    return $response;
                } catch(\Exception $e) {
                    $this->client->setSessionID($master_session_id);

                    if(strpos($e->getMessage(), 'You are not currently logged in') !== false) {
                        $email = $this->getEmailFromId($this->current_login['userID']);
                        $this->addLogin($email, $this->client->loginAs($email));
                        $this->setCurrentLogin($this->saved_logins[$email]);
                        return call_user_func_array([$this, $name], $args);
                    }

                    throw $e;
                }

            } else {
                throw new ImpersonateException('You have not set a current user to impersonate. Please call Impersonate::asUser($email) before you try to run any commands against the client.');
            }
        }

        throw new \BadMethodCallException("The method '$name' does not exist");
    }

    /**
     * @return mixed
     */
    public function getCurrentLogin()
    {
        return $this->current_login;
    }

    /**
     * @param mixed $current_login
     */
    public function setCurrentLogin($current_login)
    {
        $this->current_login = $current_login;
    }

    /**
     * Determines if the given string is an email or id. If it is an ID, it gets the email for that ID
     *
     * @param string $emailOrId
     * @return string The email for the user
     */
    protected function getEmailFromId($emailOrId)
    {
        if (isset($this->id_to_email_maps[$emailOrId])) {
            return $this->id_to_email_maps[$emailOrId];
        }
        return $emailOrId;
    }
}