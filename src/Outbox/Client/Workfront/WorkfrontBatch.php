<?php namespace Outbox\Client\Workfront;
use GuzzleHttp\BatchResults;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Pool;
use Outbox\Client\Workfront\Exceptions\WorkfrontRequestException;

/**
 * Class WorkfrontBatch
 *
 * Start of batch logic, but moved to functions on the actual client
 *
 * @package Outbox\Client\Workfront
 */

class WorkfrontBatch implements \ArrayAccess
    //class WorkfrontBatch
{
    protected $isImpersonating = false;
    protected $results = [];
    protected $errorResults = [];
    protected $successResults = [];
    /**
     * @var BatchResults
     */
    protected $responses;
    protected $errors = [];
    protected $successfull = [];
    protected $lastRequests = [];
    protected $hasRun = false;
    protected $lastResponses = [];
    protected $lastResults = [];
    /**
     * Add client to batch constructor
     *
     * @var WorkfrontClient
     */
    private $client;
    private $requests = [];
    private $atomic = true;
    private $max_concurrent = 10;
    /**
     * @var Impersonate
     */
    private $impersonate;

    public function __construct(WorkfrontClient $client, Impersonate &$impersonate = null)
    {
        $this->client = $client;
        $this->impersonate = $impersonate;
    }

    public function setMaxConcurrent($max)
    {
        $this->max_concurrent = (int) $max;
    }

    // Run the requests in the batch
    public function run(callable $onSuccess = null, callable $onError = null)
    {
        // Get query
        // Run batch with query
        $this->responses = Pool::batch($this->client->getHttpClient(),$this->requests,['pool_size' => $this->max_concurrent]);

        $this->processResponses($this->responses, $onSuccess,$onError);

        $this->hasRun = true;

        return $this;
    }

    public function __call($name, $args)
    {
        if($this->hasRun === true) {
            $this->hasRun = false;
            $this->reset();
        }

        if(! is_null($this->impersonate) && method_exists($this->impersonate, $name)) {
            $return = call_user_func_array([$this->impersonate, $name], $args);
            if($return instanceof Impersonate) {
                $this->isImpersonating = true;
                return $this;
            }

            return $return;
        } elseif(method_exists($this->client, $name)) {
            $this->setGetRequests();
            $this->requests[] = $this->getRequest($name, $args);
            $this->setGetResponses();
            return $this;
        }

        throw new \BadMethodCallException("The method '$name' does not exist");
    }

    private function setGetRequests()
    {
        if($this->isImpersonating === true) {
            $this->impersonate->getClient()->returnOnlyRequests();
            return null;
        }

        $this->client->returnOnlyRequests();
    }

    private function getRequest($name, $args)
    {
        if($this->isImpersonating === true)
        {
            return call_user_func_array([$this->impersonate, $name], $args);
        }

        return call_user_func_array([$this->client, $name], $args);
    }

    private function setGetResponses()
    {
        if($this->isImpersonating === true) {
            $this->impersonate->getClient()->returnResponses();
            $this->isImpersonating = false;
            return null;
        }

        $this->client->returnResponses();
    }

    private function processResponses(BatchResults $batchResults, callable $onSuccess = null, callable $onError = null)
    {
        foreach($batchResults as $key => $result) {
            if($result instanceof \Exception) {
                if($result instanceof BadResponseException) {
                    $result = new WorkfrontRequestException($result);
                }

                if(! is_null($onError)) {
                    $this->errorResults[$key] = $onError($result, $this->requests[$key]);
                }
                $this->results[$key] = $result;
                $this->errors[$key] = $key;
            } else {
                $response = $this->client->processResponse($result);
                if(! is_null($onSuccess)) {
                    $this->successResults[$key] = $onSuccess($response);
                }
                $this->results[$key] = $response;
                $this->successfull[$key] = $key;
            }

        }
    }

    public function hasErrors()
    {
        foreach($this->results as $result) {
            if($result instanceof \Exception) {
                return true;
            }
        }

        return false;
    }

    public function getErrors()
    {
        return array_intersect_key($this->results,$this->errors);
    }

    public function getSuccess()
    {
        return array_intersect_key($this->results,$this->successfull);
    }

    public function getRequestCount()
    {
        return count($this->requests);
    }

    public function reset()
    {
        $this->lastRequests = $this->requests;
        $this->lastResponses = $this->responses;
        $this->lastResults = $this->results;
        $this->results = [];
        $this->requests = [];
        $this->responses = [];
        $this->successfull = [];
        $this->errors = [];
        $this->errorResults = [];
        $this->successResults = [];
    }

    // Remove request from batch

    // Iterate through batch items
    /**
     * (PHP 5 >= 5.0.0)<br/>
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset)
    {
        if(isset($this->results[$offset])) {
            return true;
        }

        return false;
    }

    /**
     * (PHP 5 >= 5.0.0)<br/>
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset)
    {
        return $this->results[$offset];
    }

    /**
     * (PHP 5 >= 5.0.0)<br/>
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->results[$offset] = $value;
    }

    /**
     * (PHP 5 >= 5.0.0)<br/>
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->results[$offset]);
    }
}