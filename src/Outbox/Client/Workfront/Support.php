<?php namespace Outbox\Client\Workfront;


class Support {
    public static function isValidEmail($email)
    {
        if(filter_var($email,FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }

    public static function isValidUrl($url)
    {
        if(filter_var($url, FILTER_VALIDATE_URL)) {
            return true;
        } else {
            return false;
        }
    }
}