<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use ApiTester;
use Codeception\Lib\ModuleContainer;
use Codeception\TestInterface;
use Codeception\Util\Fixtures;
use Exception;
use Outbox\Client\Workfront\WorkfrontClient;

class ApiHelper extends \Codeception\Module
{
    private $client;
    private $username;
    private $password;
    private $instantiated_objects = [];
    private $current_result;

    public function __construct(ModuleContainer $moduleContainer, $config = null)
    {
        parent::__construct($moduleContainer, $config);
        if(isset($config) && $config['url']) {
            $this->client = new WorkfrontClient($config['url'], $config['version']);
            if(isset($config['username']) && isset($config['password'])) {
                $this->username = $config['username'];
                $this->password = $config['password'];
            }
        } else {
            $this->client = new WorkfrontClient();
        }

    }

    public function amLoggedIn($username = null, $password = null)
    {
        if(isset($username) && isset($password)) {
            $this->client->login($username, $password);
        } elseif(isset($this->username) && isset($this->password)) {
            $this->client->login($this->username, $this->password);
        }

    }

    public function haveAValidSession()
    {
        $this->assertTrue($this->client->getSessionID() !== null);
    }

    public function createObject($object_code, $params)
    {
        $object = $this->client->create($object_code, $params);
        $this->instantiated_objects[$object_code][$object['ID']] = $object['ID'];
        $this->assertNotNull($object['ID']);
        return $object;
    }

    public function getAnObject($object_code, $object_id)
    {
        $object = $this->client->get($object_code, $object_id);
        $this->assertNotNull($object['ID']);
        $this->current_result = $object;
        return $object;
    }

    public function updateObject($object_code, $object, $updates)
    {
        $return_fields = array_keys($updates);
        $return_object = $this->client->update($object_code, $object['ID'],$updates,$return_fields);
        $this->assertNotNull($return_object);
        foreach($updates as $name => $update) {
            $this->assertEquals($update,$return_object[$name]);
        }
        return $return_object;
    }

    public function deleteObject($object_code, $object_id)
    {
        $return = $this->client->delete($object_code, $object_id, true);
        unset($this->instantiated_objects[$object_code][$object_id]);
        $this->assertNotNull($return);
        $this->assertEquals($return['success'],true);
        return $return;
    }

    public function searchForAnObject($object_code, $search_params)
    {
        $return_fields = array_keys($search_params);
        $return_objects = $this->client->search($object_code,$search_params,$return_fields);
        $this->assertNotNull($return_objects);
        foreach($return_objects as $key => $object) {
            foreach($search_params as $name => $search_param) {
                $this->assertEquals($search_param,$object[$name]);
            }
        }
        return $return_objects;
    }

    public function cleanUpObjects()
    {
        foreach($this->instantiated_objects as $type => $objects) {
            foreach($objects as $key => $object_id) {
                $this->deleteObject($type, $object_id);
            }
            unset($this->instantiated_objects[$type]);
        }
    }

    public function executeNamedQuery(){
        $time = time();
        $startDate = $time - 2592000 * 36;
        $endDate = $time + 2592000 * 36; // One month * X
        $query = ['startDate' => date('Y-m-d', $startDate), 'endDate' => date('Y-m-d', $endDate), 'calendarID' => '54e3c67d00017d54a815a711185749dc'];
        $this->client->setVersion(0);
        $results = $this->client->namedQuery('calitm', 'feed', $query, ['*']);
        $this->assertTrue(sizeof($results) > 0);
        return $results;
    }

    public function seeExceptionThrown($exception, $function)
    {
        $this->assertTrue($this->ifException($exception,$function),$function);

    }

    public function ifException($exception, $function)
    {
        try
        {
            $function();
            return false;
        } catch (Exception $e) {
            var_dump(get_class($e));
            if( get_class($e) == $exception ){
                return true;
            }
            return false;
        }
    }

    public function seeNoExceptionIsThrown($function)
    {
        try
        {
            $function();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function _after(TestInterface $test)
    {
        $this->cleanUpObjects();
    }
}
