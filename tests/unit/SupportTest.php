<?php


use Outbox\Client\Workfront\Support;

class SupportTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /** @test */
    public function itValidatesUrls()
    {
        $this->assertTrue(Support::isValidUrl('http://www.example.com'));
        $this->assertTrue(Support::isValidUrl('https://www.example.com'));
        $this->assertFalse(Support::isValidUrl('sdfsdfsd'));
        $this->assertFalse(Support::isValidUrl('www.example.com'));
        $this->assertFalse(Support::isValidUrl('changed.now'));
    }

    /** @test */
    public function itValidatesEmails()
    {
        $this->assertTrue(Support::isValidEmail('email@example.com'));
        $this->assertTrue(Support::isValidEmail('email.some@example.com'));
        $this->assertFalse(Support::isValidEmail('email.some.com'));
        $this->assertFalse(Support::isValidEmail('asdfasdfasdf'));
        $this->assertFalse(Support::isValidEmail('asdfasdfasdf'));
    }

}