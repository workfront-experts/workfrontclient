<?php


use Outbox\Client\Workfront\WorkfrontClient;
use \Mockery as m;

class WorkfrontClientTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var WorkfrontClient
     */
    protected $client;

    public static $functions;

    protected function _before()
    {
        $this->client = new WorkfrontClient();
        self::$functions = m::mock();
    }

    protected function _after()
    {
        Mockery::close();
    }

    /** @test */
    public function itSetsABaseUrl()
    {
        $base_url = 'https://www.example.com';
        $this->client->setBaseUrl($base_url);

        $this->assertEquals($this->client->getBaseUrl(), $base_url);
    }

    /** @test */
    public function itSetsBlankBaseUrlToDefault()
    {
        $this->client->setBaseUrl('');
        $this->assertEquals($this->client->getBaseUrl(), WorkfrontClient::$default_base_url);
        $this->client->setBaseUrl(null);
        $this->assertEquals($this->client->getBaseUrl(), WorkfrontClient::$default_base_url);
    }

    /** @test */
    public function itSetsBlankValuesToDefault()
    {
        $this->client->setVersion(null);
        $this->assertTrue($this->client->getVersion() === 'v4.0');
        $this->client->setVersion('');
        $this->assertTrue($this->client->getVersion() === 'v4.0');
    }

    /** @test */
    public function itCanUseAnIntToSetTheVersion()
    {
        $this->client->setVersion(3);
        $this->assertTrue($this->client->getVersion() === 'v3.0');
        $this->client->setVersion(0);
        $this->assertTrue($this->client->getVersion() === 'api-internal');
    }

    /** @test */
    public function itCanUseAStringToSetTheVersion()
    {
        $this->expectOutputString('');
        $this->client->setVersion('v5.0');
        $this->assertTrue($this->client->getVersion() === 'v5.0');
        $this->client->setVersion('api-internal');
        $this->assertTrue($this->client->getVersion() === 'api-internal');
    }

    /**
     * @test
     * @expectedException           Outbox\Client\Workfront\Exceptions\WorkfrontClientException
     * @expectedExceptionMessage    There is not a version for the passed value
     */
    public function itThrowsAnExceptionIfVersionNotInRange()
    {
        $this->client->setVersion(-1);
        $this->client->setVersion(6);
    }

    /**
     * @test
     * @expectedException           Outbox\Client\Workfront\Exceptions\WorkfrontClientException
     * @expectedExceptionMessage    The passed username is not a valid email address
     */
    public function throwsAnExceptionIfTheLoginUsernameIsNotAnEmail()
    {
        $this->client->login('notAnEmail', 'somepass');
    }

    /**
     * @test
     */
    public function itSendsALoginRequestThroughPost()
    {
//        $http_mock = m::mock('GuzzleHttp\Client');
//        $response_mock = m::mock('GuzzleHttp\Message\Response');
//        $response_mock->shouldReceive('json')->once()->andReturn([
//            'data' => [
//                'sessionID' => 'something'
//            ]
//        ]);
//        $http_mock->shouldReceive('send')->once()->andReturn($response_mock);
//        $this->client->setHttpClient($http_mock);
//
//        $this->client->login('some@user.com', 'password');
    }

    /** @test */
    public function itReturnsAFullUrlWithVersion()
    {
        $base_url = 'https://www.example.com';
        $client = new WorkfrontClient($base_url);
        $client->setVersion(3);
        $this->assertEquals($client->getFullUrl(), $base_url . '/attask/api/v3.0');
        $client->setVersion('api-internal');
        $this->assertEquals($client->getFullUrl(), $base_url . '/attask/api-internal');
    }

    /** @test */
    public function itReturnsAFullApiPathWithVersion()
    {
        $client = new WorkfrontClient('https://www.example.com');
        $client->setVersion(3);
        $this->assertEquals($client->getApiPath(), '/attask/api/v3.0');
//        $client->setVersion('api-internal');
//        $this->assertEquals($client->getFullUrl(), $base_url . '/attask/api-internal');
    }

}