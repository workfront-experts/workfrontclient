<?php 
$I = new ApiTester($scenario);
$I->wantTo('delete an non existing object');
$I->amLoggedIn();
$I->seeExceptionThrown('Outbox\Client\Workfront\Exceptions\WorkfrontRequestException',function() use ($I) {
    $I->deleteObject('project', '00000000000000000000000000000000');
});
