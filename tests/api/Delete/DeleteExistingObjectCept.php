<?php 
$I = new ApiTester($scenario);
$I->wantTo('delete an object');
$I->amLoggedIn();
$object = $I->createObject('project',['name' => 'some name']);
$I->deleteObject('project',$object['ID']);