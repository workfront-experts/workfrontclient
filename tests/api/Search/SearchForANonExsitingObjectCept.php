<?php 
$I = new ApiTester($scenario);
$I->wantTo('search for a non existing object');
$I->amLoggedIn();
$objects = $I->searchForAnObject('project',['name' => 'a brand new item that does not exist']);
$I->assertEmpty($objects);
\Codeception\Util\Debug::debug($objects);