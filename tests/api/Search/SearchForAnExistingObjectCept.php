<?php 
$I = new ApiTester($scenario);
$I->wantTo('search for an existing object');
$I->amLoggedIn();
$I->createObject('project',['name' => 'A unique name']);
$objects = $I->searchForAnObject('project',['name' => 'A unique name']);
\Codeception\Util\Debug::debug($objects);