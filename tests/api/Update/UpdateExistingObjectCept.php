<?php 
$I = new ApiTester($scenario);
$I->wantTo('update an existing object (project)');
$I->amLoggedIn();
$object = $I->createObject('project',['name' => 'a valid object']);
$I->updateObject('project',$object,['name' => 'a new object']);
$I->updateObject('project',$object,['description' => 'a new description']);