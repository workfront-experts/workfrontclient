<?php 
$I = new ApiTester($scenario);
$I->wantTo('update with invalid parameters');
$I->amLoggedIn();
$object = $I->createObject('project',['name' => 'a valid object']);
$I->seeExceptionThrown('Outbox\Client\Workfront\Exceptions\WorkfrontRequestException',function() use ($I,$object) {
    $I->updateObject('project',$object,['name' => null]);
});
