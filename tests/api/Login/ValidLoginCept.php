<?php

use Codeception\Util\Fixtures;

$I = new ApiTester($scenario);
$I->wantTo('login as a valid user');
$I->amLoggedIn(Fixtures::get('username'), Fixtures::get('password'));
$I->haveAValidSession();