<?php

use Codeception\Util\Fixtures;

$I = new ApiTester($scenario);
$I->wantTo('Login as a invalid user');
$I->seeExceptionThrown('Outbox\Client\Workfront\Exceptions\WorkfrontRequestException',function() use ($I) {
    $I->amLoggedIn(Fixtures::get('username'), 'somepass');
});