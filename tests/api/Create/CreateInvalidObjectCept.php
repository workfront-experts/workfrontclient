<?php
$I = new ApiTester($scenario);
$I->wantTo('try to create an invalid object');
$I->amLoggedIn();
$I->seeExceptionThrown('Outbox\Client\Workfront\Exceptions\WorkfrontRequestException',function() use ($I) {
    $I->createObject('project',['description' => 'Something Bad']);
});
