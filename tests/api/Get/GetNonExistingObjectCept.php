<?php 
$I = new ApiTester($scenario);
$I->wantTo('get an non-existing object (project)');
$I->amLoggedIn();
$I->seeExceptionThrown('Outbox\Client\Workfront\Exceptions\WorkfrontRequestException', function() use ($I) {
    $I->getAnObject('project','00000000000000000000000000000000');
});