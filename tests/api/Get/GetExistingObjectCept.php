<?php 
$I = new ApiTester($scenario);
$I->wantTo('get an existing object (project)');
$I->amLoggedIn();
$object = $I->createObject('project',['name' => 'some name']);
$I->getAnObject('project',$object['ID']);