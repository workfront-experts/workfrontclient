<?php
$I = new ApiTester($scenario);
$I->wantTo('Execute a named query');
$I->amLoggedIn();
$objects = $I->executeNamedQuery();
\Codeception\Util\Debug::debug($objects);