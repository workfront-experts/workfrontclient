<?php
// Here you can initialize variables that will be available to your tests

use Codeception\Util\Fixtures;

$username = 'integrations+fidelity@simplus.com';
$password = 'P@55w0rd';

Fixtures::add('username', $username);
Fixtures::add('password', $password);